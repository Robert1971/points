package pl.sda14;

import pl.sda13.human;

import java.awt.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class GetDistance {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        printList ();

    }

    private static String setName() {
        System.out.print("podaj nazwę punktu ");
        String name = scanner.next();
        return name;
    }

    private static double setX() {
        System.out.print("podaj wartość x ");
        double x = scanner.nextDouble();
        return x;
    }


    private static double setY() {
        System.out.print("podaj wartość y ");
        double y = scanner.nextDouble();
        return y;
    }

    private static int pointsNumber() {
        System.out.print("podaj liczbę punktów ");
        int number = scanner.nextInt();
        return number;
    }



    private static List<Points> pointCollection(int pointsNumber) {

        List<Points> points = new ArrayList<>();
        for (int i = 0; i < pointsNumber; i++) {
            Points point=new Points(setName(), setX(), setY());
            points.add(point);
        }
        return points;
    }
    private static List<Points> sortPoints(List<Points> points){
        final Comparator<Points> pointsComparator = (o1, o2) -> (int) (Math.round(o1.getDist()) - Math.round(o2.getDist()));
        points.sort(pointsComparator);
        return points;
    }
    private static void printList (){
        List<Points> points = sortPoints(pointCollection(pointsNumber()));
        points.forEach(point-> System.out.println("Nazwa: "+ point.getName() + ", x: " + point.getX() +
                ", y: " + point.getY() + ", dystans: " + point.getDist()));
    }
}





