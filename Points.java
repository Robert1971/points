package pl.sda14;

import java.util.Scanner;

public class Points {

    private  double dist;
    private  double x;
    private  double y;
    private  String name;

    protected Points(String name, double x, double y) {
        this.x = x;
        this.y = y;
        this.name = name;
        this.dist = Math.sqrt(x * x + y * y);
    }

    public  double getDist() {
        return dist;
    }

    public  double getX() {
        return x;
    }

    public  double getY() {
        return y;
    }

    public  String getName() {
        return name;
    }

}




